-- Create a new table called 'Customers' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Messages', 'U') IS NOT NULL
DROP TABLE dbo.Messages
GO
-- Create the table in the specified schema
CREATE TABLE dbo.Messages
(
   Id        INT    NOT NULL   PRIMARY KEY, -- primary key column
   Priority      [NVARCHAR](50)  NOT NULL,
   Sender      [NVARCHAR](50)  NOT NULL,
   Recipient      [NVARCHAR](50)  NOT NULL,
   Title  [NVARCHAR](50)  NOT NULL,
   Content     [NVARCHAR](250)  NOT NULL
);
GO