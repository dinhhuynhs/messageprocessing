-- Create a new table called 'Customers' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Notifications', 'U') IS NOT NULL
DROP TABLE dbo.Notifications
GO
-- Create the table in the specified schema
CREATE TABLE dbo.Notifications
(
   Id        INT    NOT NULL   PRIMARY KEY, -- primary key column
   Title  [TYPE](50)  NOT NULL,
   Title  [NVARCHAR](50)  NOT NULL,
   Content     [NVARCHAR](250)  NOT NULL
);
GO