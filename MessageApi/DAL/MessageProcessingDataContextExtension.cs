﻿using System.Collections.Generic;
using System.Linq;
using MessageApi.Models;

namespace MessageApi.DAL
{
    public static class MessageProcessingDataContextExtension
    {
        public static void CreateSeedData
                 (this MessageProcessingDataContext context)
        {
            if (!context.Alerts.Any())
            {

                var alerts = new List<Alert>()
               {
                    new Alert()
                    {
                         Type = AlertType.Error,
                         Title = "Error title",
                         Content = "Error Content"
                    },
                    new Alert()
                    {
                         Type = AlertType.Info,
                         Title = "Info title",
                         Content = "Info Content"
                    },
                    new Alert()
                    {
                          Type = AlertType.Warning,
                         Title = "Warning title",
                         Content = "Warning Content"
                    }
               };
                context.AddRange(alerts);
            }

            context.SaveChanges();
        }
    }
}
