﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MessageApi.DAL;
using MessageApi.Models;

namespace MessageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private MessageProcessingDataContext db;

        public ValuesController(MessageProcessingDataContext context)
        {
            db = context;
        }


        /*// GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {

            return Ok(db.Alerts);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid? id)
        {
          
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return NotFound();
            }
            return Ok(alert);
        }


        // POST api/values
        [HttpPost]
        public ActionResult<Alert> Post(Alert alert)
        {
            db.Alerts.Add(alert);
            db.SaveChanges();
            return CreatedAtAction(nameof(Get), new { id = alert.Id }, alert);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid? id, Alert alert)
        {
            if(id != alert.Id)
            {
                return BadRequest();
            }
            db.Entry(alert).State = EntityState.Modified;
            db.SaveChanges();
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid? id)
        {
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return NotFound();
            }
            db.Alerts.Remove(alert);
            db.SaveChanges();
            return NoContent();
        }*/

        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid? id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult<Alert> Post(Alert item)
        {
            throw new NotImplementedException();
        }


        public Alert GetById(object id)
        {
            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        public IActionResult Put(Alert item)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Alert item)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(db.Alerts);
        }
    }
}
