﻿using System;
using System.Collections.Generic;
using MessageApi.DAL;
using MessageApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessageApi.Repository
{
    public class AlertRepository : IRepository<Alert>
    {
        private MessageProcessingDataContext db;

        public AlertRepository(MessageProcessingDataContext context)
        {
            db = context;
        }

        public bool Delete(Guid? id)
        {
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return false;
            }
            db.Alerts.Remove(alert);
            db.SaveChanges();
            return true;
        }

        public IEnumerable<Alert> Get()
        {
            return db.Alerts;
        }

        public Alert Get(Guid? id)
        {
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return null;
            }
            return alert;
        }

        public Alert Post(Alert item)
        {
            db.Alerts.Add(item);
            db.SaveChanges();
            return item;
        }

        public bool Put(Guid? id, Alert item)
        {
            if (id != item.Id)
            {
                return false;
            }
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }
    }
}
