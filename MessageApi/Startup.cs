﻿using MessageApi.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;



namespace MessageApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            var connectionString =
            "Server=localhost;Database=MessageDB;User Id=sa;Password=Dockersql123";
            services
              .AddDbContext<MessageProcessingDataContext>(o =>
                o.UseSqlServer(connectionString));

            //services.AddMvcWithDefaultRoute();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, MessageProcessingDataContext messageProcessingDataContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseStaticFiles();
            messageProcessingDataContext.CreateSeedData();
            /*app.Run(async (context) =>
            {
                await context
                       .Respnse
                       .WriteAsync("Hello World!");
            });*/

            app.UseHttpsRedirection();
            app.UseMvc();


        }
    }
}
