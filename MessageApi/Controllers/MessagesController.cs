﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MessageApi.DAL;
using MessageApi.Models;
using MessageApi.Repository;
using Newtonsoft.Json;

namespace MessageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {

        private MyMessageRepository messageRepository;

        public MessagesController(MessageProcessingDataContext context)
        {
            messageRepository = new MyMessageRepository(context);
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {

            return Ok(messageRepository.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid? id)
        {

            Message  message = messageRepository.Get(id);
            if (message == null)
            {
                return NotFound();
            }
            return Ok(message);
        }


        [HttpPost]
        public ActionResult<Message> Post(Message message)
        {
            messageRepository.Post(message);
            return CreatedAtAction(nameof(Get), new { id = message.Id }, message);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid? id, Message message)
        {

            bool isSuccess = messageRepository.Put(id, message);
            if (!isSuccess)
            {
                return BadRequest();
            }
            else
            {
                return NoContent();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid? id)
        {
            bool isSuccess = messageRepository.Delete(id);
            if (!isSuccess)
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        /*
        * @param : idString = "[\"7253dec0-3087-4582-29ab-08d6cf533e05\"]"      
        */
        [HttpPost("delete/all")]
        public IActionResult DeleteAll([FromBody] string idString)
        {
            if (idString != null)
            {
                Guid[] ids = JsonConvert.DeserializeObject<Guid[]>(idString);
                foreach (Guid id in ids)
                {
                    messageRepository.Delete(id);
                }
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
