﻿using System;
using Microsoft.EntityFrameworkCore;
using MessageApi.Models;
namespace MessageApi.DAL
{
    public class MessageProcessingDataContext : DbContext
    {
        public MessageProcessingDataContext(
                DbContextOptions<MessageProcessingDataContext> options)
                : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Alert> Alerts { get; set; }

        public DbSet<Notification> Notifications { get; set; }
    }
}
