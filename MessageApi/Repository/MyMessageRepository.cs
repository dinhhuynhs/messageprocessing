﻿using System;
using System.Collections.Generic;
using MessageApi.DAL;
using MessageApi.Models;
using Microsoft.EntityFrameworkCore;

namespace MessageApi.Repository
{
    public class MyMessageRepository : IRepository<Message>
    {
        private MessageProcessingDataContext db;

        public MyMessageRepository(MessageProcessingDataContext context)
        {
            db = context;
        }

        public bool Delete(Guid? id)
        {
            Message item = db.Messages.Find(id);
            if (item == null)
            {
                return false;
            }
            db.Messages.Remove(item);
            db.SaveChanges();
            return true;
        }

        public IEnumerable<Message> Get()
        {
            return db.Messages;
        }

        public Message Get(Guid? id)
        {
            Message item = db.Messages.Find(id);
            if (item == null)
            {
                return null;
            }
            return item;
        }

        public Message Post(Message item)
        {
            db.Messages.Add(item);
            db.SaveChanges();
            return item;
        }

        public bool Put(Guid? id, Message item)
        {
            if (id != item.Id)
            {
                return false;
            }
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }
    }
}
