﻿using System;
namespace MessageApi.Models
{
    public enum AlertType
    {
        Error, Warning, Info
    }
    public class Alert:BaseMessage
    {
        public AlertType Type { get; set; }
    }
}
