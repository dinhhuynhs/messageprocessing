﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MessageApi.Models
{
    public class Message : BaseMessage
    {
        [Required]
        [MaxLength(100)]
        public string Priority { get; set; }
        [Required]
        [MaxLength(100)]
        public string Sender { get; set; }
        [Required]
        [MaxLength(100)]
        public string Recipient { get; set; }
    }
}
