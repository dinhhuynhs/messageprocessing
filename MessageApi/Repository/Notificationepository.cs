﻿using System;
using System.Collections.Generic;
using MessageApi.DAL;
using MessageApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessageApi.Repository
{
    public class NotificationRepository : IRepository<Notification>
    {
        private MessageProcessingDataContext db;

        public NotificationRepository(MessageProcessingDataContext context)
        {
            db = context;
        }

        public bool Delete(Guid? id)
        {
            Notification item = db.Notifications.Find(id);
            if (item == null)
            {
                return false;
            }
            db.Notifications.Remove(item);
            db.SaveChanges();
            return true;
        }

        public IEnumerable<Notification> Get()
        {
            return db.Notifications;
        }

        public Notification Get(Guid? id)
        {
            Notification item = db.Notifications.Find(id);
            if (item == null)
            {
                return null;
            }
            return item;
        }

        public Notification Post(Notification item)
        {
            db.Notifications.Add(item);
            db.SaveChanges();
            return item;
        }

        public bool Put(Guid? id, Notification item)
        {
            if (id != item.Id)
            {
                return false;
            }
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }


    }
}
