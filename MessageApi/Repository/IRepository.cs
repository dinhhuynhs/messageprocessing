﻿using System;
using System.Collections.Generic;
using MessageApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace MessageApi.Repository
{
    public interface IRepository<T> where T:BaseMessage
    {
     
       IEnumerable<T> Get(); 
       T Get(Guid? id);
       T Post(T item);
       bool Put(Guid? id, T item);
       bool Delete(Guid? id);
     
    }
}
