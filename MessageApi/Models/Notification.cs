﻿using System;
namespace MessageApi.Models
{
    //[LocalizationEnum(typeof(MyResources))]
    public enum NotificationType
    {
        Member, Auth, Claim
    }
    public class Notification : BaseMessage
    {
        public NotificationType Type { get; set; }
    }
}
