﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MessageApi.DAL;
using MessageApi.Models;
using MessageApi.Repository;
using Newtonsoft.Json;

namespace MessageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {

        private NotificationRepository notificationRepository;

        public NotificationsController(MessageProcessingDataContext context)
        {
            notificationRepository = new NotificationRepository(context);
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {

            return Ok(notificationRepository.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid? id)
        {

            Notification notification = notificationRepository.Get(id);
            if (notification == null)
            {
                return NotFound();
            }
            return Ok(notification);
        }

        [HttpPost]
        public ActionResult<Notification> Post(Notification notification)
        {
            notificationRepository.Post(notification);
            return CreatedAtAction(nameof(Get), new { id = notification.Id }, notification);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid? id, Notification notification)
        {

            bool isSuccess = notificationRepository.Put(id, notification);
            if (!isSuccess)
            {
                return BadRequest();
            }
            else
            {
                return NoContent();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid? id)
        {
            bool isSuccess = notificationRepository.Delete(id);
            if (!isSuccess)
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        /*
        * @param : idString = "[\"7253dec0-3087-4582-29ab-08d6cf533e05\"]"      
        */
        [HttpPost("delete/all")]
        public IActionResult DeleteAll([FromBody] string idString)
        {
            if (idString != null)
            {
                Guid[] ids = JsonConvert.DeserializeObject<Guid[]>(idString);
                foreach (Guid id in ids)
                {
                    notificationRepository.Delete(id);
                }
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
