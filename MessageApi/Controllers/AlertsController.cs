﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MessageApi.DAL;
using MessageApi.Models;
using MessageApi.Repository;
using Newtonsoft.Json;

namespace MessageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlertsController : ControllerBase
    {

        private AlertRepository alertRepository ;

        public AlertsController(MessageProcessingDataContext context)
        {
            alertRepository = new AlertRepository(context);
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {

            return Ok(alertRepository.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(Guid? id)
        {

            Alert alert = alertRepository.Get(id);
            if (alert == null)
            {
                return NotFound();
            }
            return Ok(alert);
        }


        [HttpPost]
        public ActionResult<Alert> Post(Alert alert)
        {
            alertRepository.Post(alert);
            return CreatedAtAction(nameof(Get), new { id = alert.Id }, alert);
        }


       
        [HttpPut("{id}")]
        public IActionResult Put(Guid? id, Alert alert)
        {

            bool isSuccess = alertRepository.Put(id, alert);
            if (!isSuccess)
            {
                return BadRequest();
            }
            else
            {
                return NoContent();
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(Guid? id)
        {
            bool isSuccess = alertRepository.Delete(id);
            if (!isSuccess)
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        /*
         * @param : idString = "[\"7253dec0-3087-4582-29ab-08d6cf533e05\"]"      
         */
        [HttpPost("delete/all")]
        public IActionResult DeleteAll([FromBody] string idString)
        {
            if (idString != null)
            {
                Guid[] ids = JsonConvert.DeserializeObject<Guid[]>(idString);
                foreach (Guid id in ids)
                {
                    alertRepository.Delete(id);
                }
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

    }
}
